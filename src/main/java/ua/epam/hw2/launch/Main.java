package ua.epam.hw2.launch;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import ua.epam.hw2.company.Company;

public class Main {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
                "applicationContext.xml");
        Company company = context.getBean("company", Company.class);
        company.work();
    }
}
