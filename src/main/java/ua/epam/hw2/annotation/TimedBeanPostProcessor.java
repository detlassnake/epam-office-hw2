package ua.epam.hw2.annotation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class TimedBeanPostProcessor implements BeanPostProcessor {
    private static final Logger log = LoggerFactory.getLogger(TimedBeanPostProcessor.class);

    @Bean
    public BeanPostProcessor TimedAnnotationBeanPostProcessor() {
        return new BeanPostProcessor() {
            Map<String, Class> map = new HashMap<>();

            @Override
            public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
                Class<?> beanClass = bean.getClass();
                if (beanClass.isAnnotationPresent(Timed.class)) {
                    map.put(beanName, beanClass);
                }
                return bean;
            }

            @Override
            public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
                Class beanClass = map.get(beanName);
                if (beanClass != null) {
                    return Proxy.newProxyInstance(beanClass.getClassLoader(),
                            beanClass.getInterfaces(), (proxy, method, args) -> {
                                long nanoTimeStart = System.nanoTime();
                                Object returnValue = method.invoke(bean, args);
                                long nanoTimeEnd = System.nanoTime();
                                log.debug(beanName + "." + method.getName() + " - time in action: " + (nanoTimeEnd - nanoTimeStart) + " ns");
                                return returnValue;
                            });
                }
                return bean;
            }

        };
    }
}
