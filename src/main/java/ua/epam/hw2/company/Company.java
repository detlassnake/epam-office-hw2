package ua.epam.hw2.company;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ua.epam.hw2.developer.Developer;

@Component
public class Company {
    private Developer developer;

    @Autowired
    public Company(@Qualifier("javaDeveloper")Developer developer) {
        this.developer = developer;
    }

    public void work() {
        developer.writeCode();
    }
}
