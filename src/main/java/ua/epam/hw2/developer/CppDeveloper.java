package ua.epam.hw2.developer;

import org.springframework.stereotype.Component;

@Component
public class CppDeveloper implements Developer {
    public void writeCode() {
        System.out.println("Write c++ code");
    }
}
