package ua.epam.hw2.developer;

import org.springframework.stereotype.Component;
import ua.epam.hw2.annotation.Timed;

@Component
@Timed
public class JavaDeveloper implements Developer {

    public void writeCode() {
        System.out.println("Write java code");
    }
}


